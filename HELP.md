# Compilação

Usei o maven como ferramenta de compilação e o java 8 (é o que está instaldo em meu PC)

## Compilar o projeto
mvn clean install

## Para executar

 ```java -jar .\target\product-ms-1.0.00.jar```

Criei um arquivo para testar os endpoints com o Postman, caso precisem

```./Compasso UOL.postman_collection.json```

Estes mesmos testes foram escritos com JUnit e rodam durante a compilação
