package uol.compasso.desafio.alexandremattje.productms.api;

import org.apache.logging.log4j.util.Strings;

import java.util.List;

public class ValidatorStringField {

	public static void isEmpty(List<ValidationError> errors, String fieldName, String value) {
		if (Strings.isBlank(value)) {
			errors.add(ValidationError.builder().field(fieldName).message("Required field").build());
		}
	}
}
