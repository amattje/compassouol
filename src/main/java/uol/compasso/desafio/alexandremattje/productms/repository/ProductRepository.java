package uol.compasso.desafio.alexandremattje.productms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uol.compasso.desafio.alexandremattje.productms.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	long countById(Long id);
}
