package uol.compasso.desafio.alexandremattje.productms.service;

import uol.compasso.desafio.alexandremattje.productms.api.ValidationError;
import uol.compasso.desafio.alexandremattje.productms.api.ValidatorDoubleField;
import uol.compasso.desafio.alexandremattje.productms.api.ValidatorStringField;
import uol.compasso.desafio.alexandremattje.productms.model.MProduct;
import uol.compasso.desafio.alexandremattje.productms.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductValidation {

	public static ProductValidation newInstance() {
		return new ProductValidation();
	}

	public List<ValidationError> validate(Product product) {
		List<ValidationError> errors = new ArrayList<>();
		ValidatorStringField.isEmpty(errors, MProduct.meta.description.getAlias(), product.getDescription());
		ValidatorStringField.isEmpty(errors, MProduct.meta.name.getAlias(), product.getName());
		ValidatorDoubleField.isEmpty(errors, MProduct.meta.price.getAlias(), product.getPrice());
		ValidatorDoubleField.biggerThanZero(errors, MProduct.meta.price.getAlias(), product.getPrice());
		return errors;
	}

}
