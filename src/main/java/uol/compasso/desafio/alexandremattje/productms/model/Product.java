package uol.compasso.desafio.alexandremattje.productms.model;

import br.ufsc.bridge.metafy.Metafy;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder(toBuilder = true)
@Entity
@Metafy
@NoArgsConstructor
@AllArgsConstructor
public class Product {

	private static final String PRODUCT_SEQ_NAME = "product_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PRODUCT_SEQ_NAME)
	@SequenceGenerator(name = PRODUCT_SEQ_NAME, sequenceName = PRODUCT_SEQ_NAME, allocationSize = 1, initialValue = 1)
	private Long id;

	@NotNull
	private String name;
	@NotNull
	private String description;
	@NotNull
	private Double price;

}
