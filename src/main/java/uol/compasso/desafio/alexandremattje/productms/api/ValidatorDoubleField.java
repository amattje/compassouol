package uol.compasso.desafio.alexandremattje.productms.api;

import java.util.List;

public class ValidatorDoubleField {

	public static void isEmpty(List<ValidationError> errors, String fieldName, Double value) {
		if (value == null) {
			errors.add(ValidationError.builder().field(fieldName).message("Required field").build());
		}
	}

	public static void biggerThanZero(List<ValidationError> errors, String fieldName, Double value) {
		if (value != null && value < 0) {
			errors.add(ValidationError.builder().field(fieldName).message("Value must be bigger than 0.00 (zero)").build());
		}
	}
}
