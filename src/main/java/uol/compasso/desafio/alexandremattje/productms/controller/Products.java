package uol.compasso.desafio.alexandremattje.productms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uol.compasso.desafio.alexandremattje.productms.api.ValidationError;
import uol.compasso.desafio.alexandremattje.productms.api.ValidationErrorHelper;
import uol.compasso.desafio.alexandremattje.productms.model.Product;
import uol.compasso.desafio.alexandremattje.productms.service.ProductQueryParam;
import uol.compasso.desafio.alexandremattje.productms.service.ProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("products")
public class Products {

	@Autowired
	private ProductService productService;

	@PostMapping
	public ResponseEntity createNewProduct(@RequestBody Product product) {
		List<ValidationError> errors = this.productService.validate(product);
		if (errors.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CREATED).body(this.productService.save(product));
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ValidationErrorHelper.mapping(errors));
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity updateProduct(@PathVariable Long id, @RequestBody Product product) {
		if (this.productService.isPresent(id)) {
			List<ValidationError> errors = this.productService.validate(product);
			if (errors.isEmpty()) {
				product.setId(id);
				return ResponseEntity.ok(this.productService.save(product));
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ValidationErrorHelper.mapping(errors));
			}
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity loadProduct(@PathVariable Long id) {
		Optional<Product> productOptional = this.productService.find(id);
		if (productOptional.isPresent()) {
			return ResponseEntity.ok(productOptional.get());
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping
	public ResponseEntity findAll() {
		return ResponseEntity.ok(this.productService.findAll());
	}

	@GetMapping("/search")
	public ResponseEntity searchBy(
			@RequestParam(required = false) Double min_price,
			@RequestParam(required = false) Double max_price,
			@RequestParam(required = false) String q) {
		return ResponseEntity.ok(this.productService.query(ProductQueryParam.builder()
				.minPrice(min_price)
				.maxPrice(max_price)
				.query(q)
				.build()));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		if (this.productService.isPresent(id)) {
			this.productService.delete(id);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
