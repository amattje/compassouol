package uol.compasso.desafio.alexandremattje.productms.api;

import lombok.Builder;
import org.springframework.http.HttpStatus;

@Builder
public class ErrorResponse {

	private HttpStatus status_code;
	private String message;

}
