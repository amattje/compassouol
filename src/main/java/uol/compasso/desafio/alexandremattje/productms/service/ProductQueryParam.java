package uol.compasso.desafio.alexandremattje.productms.service;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProductQueryParam {

    private Double minPrice;
    private Double maxPrice;
    private String query;

}
