package uol.compasso.desafio.alexandremattje.productms.service;

import com.google.common.base.Strings;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uol.compasso.desafio.alexandremattje.productms.api.ValidationError;
import uol.compasso.desafio.alexandremattje.productms.model.Product;
import uol.compasso.desafio.alexandremattje.productms.model.QProduct;
import uol.compasso.desafio.alexandremattje.productms.repository.ProductRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Component
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private EntityManager entityManager;

	public List<ValidationError> validate(Product product) {
		return ProductValidation.newInstance().validate(product);
	}

	public Product save(Product product) {
		return this.productRepository.save(product);
	}

	public boolean isPresent(Long id) {
		return this.productRepository.countById(id) > 0;
	}

	public Optional<Product> find(Long id) {
		return this.productRepository.findById(id);
	}

	public List<Product> findAll() {
		return this.productRepository.findAll();
	}

	public List<Product> query(ProductQueryParam param) {
		JPAQuery<Product> jpaQuery = new JPAQuery<>(entityManager);
		jpaQuery.from(QProduct.product);
		if (!Strings.isNullOrEmpty(param.getQuery())) {
			jpaQuery.where(QProduct.product.description.like("%" + param.getQuery() + "%"));
		}
		if (param.getMaxPrice() != null) {
			jpaQuery.where(QProduct.product.price.loe(param.getMaxPrice()));
		}
		if (param.getMinPrice() != null) {
			jpaQuery.where(QProduct.product.price.goe(param.getMinPrice()));
		}
		return jpaQuery.fetch();
	}

	public void delete(Long id) {
		this.productRepository.deleteById(id);
	}
}
