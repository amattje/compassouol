package uol.compasso.desafio.alexandremattje.productms.api;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ValidationError {

	private String field;
	private String message;

}
