package uol.compasso.desafio.alexandremattje.productms.api;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationErrorHelper {

	public static final String STATUS_CODE = "status_code";
	public static final String MESSAGE = "message";

	public static String stringifyErrors(List<ValidationError> errors) {

		StringBuffer errorsMessage = new StringBuffer();
		errors.forEach(it -> errorsMessage.append("[" + it.getField() + ": " + it.getMessage() + "] "));

		return errorsMessage.toString();

	}

	public static Map<String, Object> mapping(List<ValidationError> errors) {
		Map<String, Object> errorMapped = new HashMap<>();
		errorMapped.put(STATUS_CODE, HttpStatus.BAD_REQUEST.value());
		errorMapped.put(MESSAGE, ValidationErrorHelper.stringifyErrors(errors));
		return errorMapped;
	}
}
