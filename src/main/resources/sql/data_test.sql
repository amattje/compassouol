insert into product (id, description, name, price) values (100, 'description for search one with price = 50', 'search one', 50);
insert into product (id, description, name, price) values (101, 'description for search two with price = 100', 'search two', 100);

insert into product (id, description, name, price) values (102, 'product for update test', 'update test', 100);

insert into product (id, description, name, price) values (103, 'product for load by id', 'load by id', 100);

insert into product (id, description, name, price) values (104, 'product for delete test', 'delete test', 100);