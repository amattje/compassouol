 drop table if exists product CASCADE;
 drop sequence if exists product_seq;
 create sequence product_seq start with 1 increment by 1;
 create table product (id bigint not null, description varchar(255) not null, name varchar(255) not null, price double not null, primary key (id));