package uol.compasso.desafio.alexandremattje.productms.controller;


import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import uol.compasso.desafio.alexandremattje.productms.ProductMsApplication;
import uol.compasso.desafio.alexandremattje.productms.model.MProduct;
import uol.compasso.desafio.alexandremattje.productms.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProductMsApplication.class })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTests {

    @LocalServerPort
    private int port;
    
    @Test
    public void successfulCreateProduct() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products";
        HttpEntity<Product> request = new HttpEntity(Product.builder()
                .description("descrição")
                .name("nome")
                .price(59.99D)
                .build());

        ResponseEntity<Product> response = restTemplate.postForEntity(productsUrl, request, Product.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
        Product responseProduct = response.getBody();

        assertThat(responseProduct, not(nullValue()));
        assertThat(responseProduct.getId(), not(nullValue()));
        assertThat(responseProduct.getDescription(), equalTo("descrição"));
        assertThat(responseProduct.getName(), equalTo("nome"));
        assertThat(responseProduct.getPrice(), equalTo(59.99D));
    }

    @Test
    public void allValidateFailedCreateProduct() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products";
        HttpEntity<Product> request = new HttpEntity<>(new Product());

        try {
            restTemplate.postForObject(productsUrl, request, Product.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getRawStatusCode(), equalTo(HttpStatus.BAD_REQUEST.value()));
            assertThat(e.getMessage(), equalTo("400 : [{\"status_code\":400,\"message\":\"[description: Required field] [name: Required field] [price: Required field] \"}]"));
        }
    }

    @Test
    public void successfulUpdateProduct() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/102";
        HttpEntity<Product> request = new HttpEntity(Product.builder()
                .description("product description updated")
                .name("updated")
                .price(1.99D)
                .build());

        ResponseEntity<Product> response = restTemplate.exchange(productsUrl, HttpMethod.PUT, request, Product.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        Product responseProduct = response.getBody();

        assertThat(responseProduct, not(nullValue()));
        assertThat(responseProduct.getId(), equalTo(102L));
        assertThat(responseProduct.getDescription(), equalTo("product description updated"));
        assertThat(responseProduct.getName(), equalTo("updated"));
        assertThat(responseProduct.getPrice(), equalTo(1.99D));
    }

    @Test
    public void notFoundFailUpdateProduct() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/999";
        HttpEntity<Product> request = new HttpEntity(new Product());

        try {
            restTemplate.put(productsUrl, request);
        } catch (HttpClientErrorException e) {
            assertThat(e.getRawStatusCode(), equalTo(HttpStatus.NOT_FOUND.value()));
            assertThat(e.getMessage(), equalTo("404 : [no body]"));
        }
    }

    @Test
    public void allValidateFailedUpdateProduct() throws JSONException {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/102";
        HttpEntity<Product> request = new HttpEntity<>(new Product());

        try {
            restTemplate.put(productsUrl, request);
        } catch (HttpClientErrorException e) {
            assertThat(e.getRawStatusCode(), equalTo(HttpStatus.BAD_REQUEST.value()));
            assertThat(e.getMessage(), equalTo("400 : [{\"status_code\":400,\"message\":\"[description: Required field] [name: Required field] [price: Required field] \"}]"));
        }
    }

    @Test
    public void loadProductId103() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/103";

        ResponseEntity<Product> response = restTemplate.getForEntity(productsUrl, Product.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        Product responseProduct = response.getBody();

        assertThat(responseProduct, not(nullValue()));
        assertThat(responseProduct.getId(), equalTo(103L));
        assertThat(responseProduct.getDescription(), equalTo("product for load by id"));
        assertThat(responseProduct.getName(), equalTo("load by id"));
        assertThat(responseProduct.getPrice(), equalTo(100D));
    }

    @Test
    public void notFoundLoadProduct() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/999";

        try {
            restTemplate.getForEntity(productsUrl, Product.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getRawStatusCode(), equalTo(HttpStatus.NOT_FOUND.value()));
            assertThat(e.getMessage(), equalTo("404 : [no body]"));
        }
    }

    @Test
    public void findAllProduct() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products";

        ResponseEntity<List> response = restTemplate.getForEntity(productsUrl, List.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<Product> responseProduct = new ArrayList<>(Objects.requireNonNull(response.getBody()));

        assertThat(responseProduct.size(), greaterThan(0));
    }

    @Test
    public void searchByNameAndDescription() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/search";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(productsUrl)
                .queryParam("q", "search");

        ResponseEntity<List> response = restTemplate.getForEntity(builder.toUriString(), List.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<Map<String, String>> responseProduct = Objects.requireNonNull(response.getBody());

        assertThat(responseProduct.size(), greaterThan(0));
        responseProduct.forEach(it -> {
            assertThat(it.get(MProduct.meta.description.getAlias()), containsString("search"));
            assertThat(it.get(MProduct.meta.name.getAlias()), containsString("search"));
        });
    }

    @Test
    public void searchByPriceGreaterThanOrEqual50() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/search";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(productsUrl)
                .queryParam("min_price", 50D);

        ResponseEntity<List> response = restTemplate.getForEntity(builder.toUriString(), List.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<Map<String, Double>> responseProduct = Objects.requireNonNull(response.getBody());

        assertThat(responseProduct.size(), greaterThan(0));
        responseProduct.forEach(it -> {
            assertThat(it.get(MProduct.meta.price.getAlias()), greaterThanOrEqualTo(50D));
        });
    }

    @Test
    public void searchByPriceLessThanOrEqual50() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/search";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(productsUrl)
                .queryParam("max_price", 50D);

        ResponseEntity<List> response = restTemplate.getForEntity(builder.toUriString(), List.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<Map<String, Double>> responseProduct = Objects.requireNonNull(response.getBody());

        assertThat(responseProduct.size(), greaterThan(0));
        responseProduct.forEach(it -> {
            assertThat(it.get(MProduct.meta.price.getAlias()), lessThanOrEqualTo(50D));
        });
    }

    @Test
    public void searchByPriceLessThanOrEqual75AndGreaterThanOrEqual50() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/search";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(productsUrl)
                .queryParam("q", "search")
                .queryParam("max_price", 75D)
                .queryParam("min_price", 50D);

        ResponseEntity<List> response = restTemplate.getForEntity(builder.toUriString(), List.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<Map<String, Object>> responseProduct = Objects.requireNonNull(response.getBody());

        assertThat(responseProduct.size(), greaterThan(0));
        responseProduct.forEach(it -> {
            assertThat((String) it.get(MProduct.meta.description.getAlias()), containsString("search"));
            assertThat((String) it.get(MProduct.meta.name.getAlias()), containsString("search"));
            assertThat((Double) it.get(MProduct.meta.price.getAlias()), lessThanOrEqualTo(75D));
            assertThat((Double) it.get(MProduct.meta.price.getAlias()), greaterThanOrEqualTo(50D));
        });
    }

    @Test
    public void deleteNotFound() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/999";

        try {
            restTemplate.delete(productsUrl);
        } catch (HttpClientErrorException e) {
            assertThat(e.getRawStatusCode(), equalTo(HttpStatus.NOT_FOUND.value()));
            assertThat(e.getMessage(), equalTo("404 : [no body]"));
        }
    }

    @Test
    public void delete() {
        RestTemplate restTemplate = new RestTemplate();
        String productsUrl = "http://localhost:" + port + "/products/104";

        restTemplate.delete(productsUrl);

        try {
            restTemplate.getForEntity(productsUrl, Product.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getRawStatusCode(), equalTo(HttpStatus.NOT_FOUND.value()));
            assertThat(e.getMessage(), equalTo("404 : [no body]"));
        }
    }
}
