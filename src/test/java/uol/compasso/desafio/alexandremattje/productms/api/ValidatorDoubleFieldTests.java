package uol.compasso.desafio.alexandremattje.productms.api;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ValidatorDoubleFieldTests {

    @Test
    public void isEmptyValidation() {
        List<ValidationError> errors = new ArrayList<>();
        ValidatorDoubleField.isEmpty(errors, "fieldTest", null);

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getMessage(), equalTo("Required field"));
        assertThat(errors.get(0).getField(), equalTo("fieldTest"));
    }

    @Test
    public void isBiggerThanZeroValidation() {
        List<ValidationError> errors = new ArrayList<>();
        ValidatorDoubleField.biggerThanZero(errors, "fieldTest", -1D);

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getMessage(), equalTo("Value must be bigger than 0.00 (zero)"));
        assertThat(errors.get(0).getField(), equalTo("fieldTest"));
    }

    @Test
    public void isOk() {
        List<ValidationError> errors = new ArrayList<>();
        ValidatorDoubleField.biggerThanZero(errors, "fieldTest", 9.99);
        ValidatorDoubleField.isEmpty(errors, "fieldTest", 9.99);

        assertThat(errors.size(), equalTo(0));
    }

}
