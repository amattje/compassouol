package uol.compasso.desafio.alexandremattje.productms.api;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ValidatorStringFieldTests {

    @Test
    public void isEmptyValidation() {
        List<ValidationError> errors = new ArrayList<>();
        ValidatorStringField.isEmpty(errors, "fieldTest", "");

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getMessage(), equalTo("Required field"));
        assertThat(errors.get(0).getField(), equalTo("fieldTest"));
    }

    @Test
    public void isNullValidation() {
        List<ValidationError> errors = new ArrayList<>();
        ValidatorStringField.isEmpty(errors, "fieldTest", null);

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getMessage(), equalTo("Required field"));
        assertThat(errors.get(0).getField(), equalTo("fieldTest"));
    }

    @Test
    public void isOk() {
        List<ValidationError> errors = new ArrayList<>();
        ValidatorStringField.isEmpty(errors, "fieldTest", "ok");

        assertThat(errors.size(), equalTo(0));
    }

}
