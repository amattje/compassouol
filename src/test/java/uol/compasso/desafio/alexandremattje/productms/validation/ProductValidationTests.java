package uol.compasso.desafio.alexandremattje.productms.validation;

import org.junit.jupiter.api.Test;
import uol.compasso.desafio.alexandremattje.productms.api.ValidationError;
import uol.compasso.desafio.alexandremattje.productms.model.MProduct;
import uol.compasso.desafio.alexandremattje.productms.model.Product;
import uol.compasso.desafio.alexandremattje.productms.service.ProductValidation;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ProductValidationTests {

    @Test
    public void fieldDescriptionRequired() {
        List<ValidationError> errors = ProductValidation.newInstance().validate(Product.builder()
                .price(9D)
                .name("any value")
                .build());

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getField(), equalTo(MProduct.meta.description.getAlias()));
        assertThat(errors.get(0).getMessage(), equalTo("Required field"));
    }

    @Test
    public void fieldNameRequired() {
        List<ValidationError> errors = ProductValidation.newInstance().validate(Product.builder()
                .price(9D)
                .description("any value")
                .build());

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getField(), equalTo(MProduct.meta.name.getAlias()));
        assertThat(errors.get(0).getMessage(), equalTo("Required field"));
    }

    @Test
    public void fieldPriceRequired() {
        List<ValidationError> errors = ProductValidation.newInstance().validate(Product.builder()
                .description("any value")
                .name("any value")
                .build());

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getField(), equalTo(MProduct.meta.price.getAlias()));
        assertThat(errors.get(0).getMessage(), equalTo("Required field"));
    }

    @Test
    public void fieldPriceGreaterThanOrEqualToZero() {
        List<ValidationError> errors = ProductValidation.newInstance().validate(Product.builder()
                .price(-9D)
                .description("any value")
                .name("any value")
                .build());

        assertThat(errors.size(), equalTo(1));
        assertThat(errors.get(0).getField(), equalTo(MProduct.meta.price.getAlias()));
        assertThat(errors.get(0).getMessage(), equalTo("Value must be bigger than 0.00 (zero)"));
    }

    @Test
    public void allFieldsRequired() {
        List<ValidationError> errors = ProductValidation.newInstance().validate(Product.builder().build());

        assertThat(errors.size(), equalTo(3));
        errors.forEach(it -> {
            assertThat(it.getMessage(), equalTo("Required field"));
        });
    }


}
